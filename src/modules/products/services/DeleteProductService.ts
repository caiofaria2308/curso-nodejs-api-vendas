import AppError from '@shared/errors/AppError';
import { getCustomRepository } from 'typeorm';
import { ProductRepository } from '../typeorm/repositories/ProductsRepository';

interface IRequest {
  id: string;
}

class DeleteProductService {
  public async execute({ id }: IRequest): Promise<void> {
    const rep = getCustomRepository(ProductRepository);
    const product = await rep.findOne(id);
    if (!product) {
      throw new AppError('Product not found');
    }
    await rep.delete(product);
  }
}

export default DeleteProductService;
