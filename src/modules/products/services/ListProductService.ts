import { getCustomRepository } from 'typeorm';
import Product from '../typeorm/entities/Product';
import { ProductRepository } from '../typeorm/repositories/ProductsRepository';

class ListProductService {
  public async execute(): Promise<Product[]> {
    const rep = getCustomRepository(ProductRepository);
    const products = await rep.find();
    return products;
  }
}

export default ListProductService;
