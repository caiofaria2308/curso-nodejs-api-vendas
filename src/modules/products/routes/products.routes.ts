import { Router } from 'express';
import ProductsController from '../controllers/ProductsControllers';

const productsRouter = Router();
const controller = new ProductsController();
productsRouter.get('/', controller.index);
productsRouter.post('/', controller.create);
productsRouter.get('/:id', controller.show);
productsRouter.put('/:id', controller.update);
productsRouter.delete('/:id', controller.delete);

export default productsRouter;
